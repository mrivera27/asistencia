package cr.ac.ucenfotec.figuras.ui;

import cr.ac.ucenfotec.figuras.bl.Persona;
import cr.ac.ucenfotec.figuras.bl.Rectangulo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public static void main(String[] args) {
        Rectangulo rectangulo = new Rectangulo(20, 10);
        Rectangulo rectanguloGrande = new Rectangulo(500, 250);

        out.println(rectangulo.toString());
        out.println("El perimetro es: " + rectangulo.calcPerimetro());
        out.println("El area es: " + rectangulo.calcArea());

        out.println("*************");

        out.println(rectanguloGrande.toString());
        out.println("El perimetro es: " + rectanguloGrande.calcPerimetro());
        out.println("El area es: " + rectanguloGrande.calcArea());

        out.println("*************");

        Persona persona = new Persona("Maria Jose");
        out.println(persona.toString());
    }
}
