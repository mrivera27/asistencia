package cr.ac.ucenfotec.figuras.bl;

public class Rectangulo {
    private double lado;
    private double altura;

    // Constructor vacio
    public Rectangulo() {
        this.lado = 10;
        this.altura = 5;
    }

    // Constructor con parametros
    public Rectangulo(int lado, int altura) {
        this.lado = lado;
        this.altura = altura;
    }

    // Setters
    public void setLado(double lado) {
        this.lado = lado;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    // Getters
    public double getLado() {
        return this.lado;
    }

    public double getAltura() {
        return this.altura;
    }

    // Perimetro
    public double calcPerimetro() {
        return 2 * (getLado() + getAltura());
    }

    // Area
    public double calcArea() {
        return getLado() + getAltura();
    }

    @Override
    public String toString() {
        return "Mi altura es: " + this.altura + "cm\nMi lado es: " + this.lado + "cm";
    }
}
