package cr.ac.ucenfotec.figuras.bl;

public class Persona {
    private String nombre;

    public Persona() {
        this.nombre = "Sin nombre";
    }

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Mi nombre es " + this.nombre;
    }
}